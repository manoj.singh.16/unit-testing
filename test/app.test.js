const chai = require('chai');
const { fizzBuzz, fibonacci } = require('../app');
chai.use(require('chai-things'));

const { expect } = chai;

describe('App', () => {
  describe('FizzBuzz problem result', () => {
    const number = 150;
    const fizzBuzzResult = fizzBuzz(number);
    describe('check for invalid inputs', () => {
      it('should be empty array when passed nothing', () => {
        expect(fizzBuzz()).to.be.empty.and.to.be.an('array');
      });

      it('should be empty array when passed no less than 1', () => {
        expect(fizzBuzz(-1)).to.be.empty.and.to.be.an('array');
      });

      it('should be empty array when passed non int', () => {
        expect(fizzBuzz('random text')).to.be.empty.and.to.be.an('array');
      });
    });

    describe('check for valid inputs', () => {
      it('should have equal length to pass number', () => {
        expect(fizzBuzzResult.length).to.be.eq(number);
      });

      it('should have string FizzBuzz in place of number divisible by 15', () => {
        expect(fizzBuzzResult.filter((result, index) => (index + 1) % 15 === 0)).all.to.be.equal('FizzBuzz');
      });
      it('should have string Fizz in place of number divisible by 3', () => {
        expect(fizzBuzzResult.filter((result, index) => (index + 1) % 3 === 0 && (index + 1) % 5 !== 0)).all.to.be.equal('Fizz');
      });
      it('should have string Buzz in place of number divisible by 5', () => {
        expect(fizzBuzzResult.filter((result, index) => (index + 1) % 5 === 0 && (index + 1) % 3 !== 0)).all.to.be.equal('Buzz');
      });
    });
  });

  describe('Fibonacci problem result', () => {
    describe('check for invalid inputs', () => {
      it('should return 0 when passed 0', () => {
        expect(fibonacci(0)).to.be.eq(0);
      });

      it('should throw error', () => {
        expect(() => fibonacci('something')).to.throw(Error);
      });
      it('should throw error ', () => {
        expect(() => fibonacci(-1)).to.throw(Error);
      });
    });

    describe('check for valid inputs', () => {
      it('should generate 5', () => {
        expect(fibonacci(5)).to.be.eq(5);
      });
      it('should generate 55', () => {
        expect(fibonacci(10)).to.be.eq(55);
      });
      it('should generate 6765', () => {
        expect(fibonacci(20)).to.be.eq(6765);
      });
      it('should generate 75025', () => {
        expect(fibonacci(25)).to.be.eq(75025);
      });
    });
  });
});
