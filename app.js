const fizzBuzz = (number) => {
  const result = [];
  for (let currentNumber = 1; currentNumber <= number; currentNumber++) {
    if (currentNumber % 15 === 0) {
      result.push('FizzBuzz');
    } else if (currentNumber % 3 === 0) {
      result.push('Fizz');
    } else if (currentNumber % 5 === 0) {
      result.push('Buzz');
    } else {
      result.push(currentNumber);
    }
  }

  return result;
};

const cache = {};
const fibonacci = (number) => {
  if (!Number.isInteger(number) || number < 0) {
    throw new Error('Invalid inputt');
  } else if (number <= 1) {
    return number;
  } else {
    if (cache[number]) {
      return cache[number];
    } else {
      cache[number] = fibonacci(number - 1) + fibonacci(number - 2);
      return cache[number];
    }
  }
};

module.exports = {
  fizzBuzz,
  fibonacci,
};
